package fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unitaire du DAO d'accès au Wallet de Badges digitaux
 */
public class JSONWalletFrameTest {

    private static final Logger LOG = LogManager.getLogger(JSONWalletFrameTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.json");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * Teste l'initialisation du Wallet
     */
    @Test
    public void testGetEncodedImageOutputWhenDatabaseIsEmpty() throws IOException {


        try(RandomAccessFile directFile = new RandomAccessFile(walletDatabase, "rw");
            BufferedReader reader = new BufferedReader(new FileReader(walletDatabase)))
        {
            JSONWalletFrame wallet = new JSONWalletFrame(directFile);
            wallet.getEncodedImageOutput();

            JsonFactory jsonFactory = new JsonFactory();
            jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
            ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

            String targetAmorceJson = "[{\"badge\":{\"metadata\":{\"badgeId\":1,\"walletPosition\":0,\"imageSize\":-1}}},{\"payload\":null}]";
            DigitalBadge targetAmorce = objectMapper.readValue(targetAmorceJson.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);

            String actualAmorceJson = reader.readLine();
            DigitalBadge actualAmorce = objectMapper.readValue(actualAmorceJson.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);

            assertEquals(targetAmorce, actualAmorce);
        }

    }

    /**
     * Teste la récupéreration d'un Badge à partir de Métadonnées incohérente
     */
    @Test
    public void testGetBadgeFromDatabaseByMetadataWithFraud(){
        try {
            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadge expectedBadge1 = new DigitalBadge("SUPER_STAR", begin, end, new DigitalBadgeMetadata(1, 0,557), null);

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");

            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            assertThrows(IOException.class, () -> {
                dao.getBadgeFromMetadata(fileBadgeStream1, expectedBadge1);
            });



        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }


    /**
     * Teste l'initialisation du Wallet
     */
    @Test
    public void testGetEncodedImageOutput() throws IOException {

        try(RandomAccessFile directFile = new RandomAccessFile(RESOURCES_PATH+ "db_wallet.json", "rw")){
            JSONWalletFrame wallet = new JSONWalletFrame(directFile);
            wallet.getEncodedImageOutput();
            assertEquals(3, wallet.getNumberOfLines());
        }
    }

}
